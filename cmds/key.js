/* ----------------------------------------
	This file controls all the stuff related
	to the onboarding process of giving keys
	to new users via vote/admin powers.
 ---------------------------------------- */
require("dotenv").config({silent: true});

let logger  = require("../core/logger"),
	command = require("../core/command").Command,
	TOKEN 	= require("../core/tokens"),
	dio		= require("../core/dio"),
	help	= require("../core/helpers"),
	x 		= require("../core/vars"),
	cmdKey	= null,
	cmdNoKey = null,
	cmdWave	= null;

// If there is no FB token (localhost), ABORT!
if (!TOKEN.FBPKEY()) {
	cmdKey = new command("key", ["!key","!giveticket"], "Adds a vote to key a member/instakey by dev", function() {
		logger.log("Firebase tokens are busted.", "Warn");
		//logger.log(TOKEN.FBPKEYID(), "Warn");
	});

	cmdNoKey = new command("key", ["!nokey","!taketicket"], "Blocks a vote to key a member", function() {
		logger.log("Firebase tokens are busted.", "Warn");
		//logger.log(TOKEN.FBPKEYID(), "Warn");
	});

	cmdWave = new command("key", "!wave", "Checks the current wave of lotto entrees", function() {
		logger.log("Firebase tokens are busted.", "Warn");
	});
} else {
	// =================
	// Firebase for keys
	// =================
	let fba = require("firebase-admin"),
		keyFire = fba.initializeApp({
			credential: fba.credential.cert(x.firebasekeycfg),
			databaseURL: "https://ltf-alpha-keys.firebaseio.com"
		}),
		keys = keyFire.database().ref("key");

	logger.log("Private Firebase initialized!", logger.MESSAGE_TYPE.OK);
	// =================

	let giveKey = function(data,lucky,key,mod=false) {
		logger.log(`Commence key giving to <@${lucky}>`, "Info");

		let whodidthat = (mod) ? "the Moderators' vote" : "one of the Devs";
		// Direct message
		dio.say(`Hey <@${lucky}>, looks like you've been bestowed a key by ${whodidthat}! :key: \n
		Your Tooth and Tail steam key is: \`${key}\`! We hope you enjoy. :smile: Click here to activate it on steam: steam://open/activateproduct \n
		Please make sure to give us feedback in chat! You can mark yourself with \`!ready\`
		to let fellow players know you're up for a game in the #community channel.`,data,lucky);

		logger.log(`<@${lucky}> received ${key}`, logger.MESSAGE_TYPE.OK);

		// Completion message
		dio.say(`:tada: <@${lucky}> has now joined the alpha. :tada:`,data,x.chan);
		//regPlayer(lucky,"");

		// Remove from new
		data.bot.removeFromRole({
			serverID: x.chan,
			userID: lucky,
			roleID: x.noob
		}, function(err,resp) {
			if (err) logger.log(`${err} / ${resp}`, logger.MESSAGE_TYPE.Error);
		});

		// Add to members
		setTimeout(function() {
			data.bot.addToRole({
				serverID: x.chan,
				userID: lucky,
				roleID: x.member
			}, function(err, resp) {
				if (err) logger.log(`${err} / ${resp}`, "Error");
			});
		}, 500);
	};

	let voteKey = function(data, vote) {
		let uRoles = data.bot.servers[x.chan].members[data.userID].roles,
			fromID = data.userID,
			from = data.user,
			lucky = help.getUser(data.message),
			memsnap = data.bot.servers[x.chan].members;

		// Ignore if a non-mod/admin attempts to key
		if (!uRoles.includes(x.mod) && !uRoles.includes(x.admin)) {
			dio.del(data.messageID, data);
			let v = [
				"🕑 Nice try sneaky pants.",
				"🕑 Nope.",
				"🕑 You are neither an admin, nor mod, be gone, peasant.`",
				"🕑 I'll just ignore that...",
				"🕑 I can't hear you!!",
				"🕑 Did you hear something?"
			];
			let n = Math.floor( Math.random()*v.length );
			dio.say(v[n], data);
			return false;
		}

		// Check @user
		if (!memsnap[lucky]) {
			dio.del(data.messageID, data);
			dio.say("🕑 Hmm, that user doesn't exist. Did you @ them correctly?", data);
			return false;
		}

		// Already key'd?
		if ( memsnap[lucky].roles.includes(x.member) ) {
			dio.del(data.messageID, data);
			dio.say("🕑 This user is a member and should have a :key: already!", data);
			return false;
		}

		logger.log(`${from} is voting on a key for userID: ${lucky}.\n Server user: ${data.bot.servers[x.chan].members[lucky].username} \n Memsnap: ${memsnap[lucky].username}`, logger.MESSAGE_TYPE.OK);

		// Grab last key in the list
		keys.orderByKey().limitToLast(1).once("value", function(snapshot) {
			//let k = snapshot.key;
			let kk = snapshot.val();
			//console.log(k,kk,lucky);

			// Break if no keys left
			if (!kk) {
				dio.del(data.messageID, data);
				dio.say(`🕑 Welp. This is awkward <@${fromID}>. We need to refill the key list. Sorry <@${lucky}>, please standby!`, data);
				logger.log("Keys need to be refilled?", "Error");
				return false;
			}

			// Mods Vote
			if (uRoles.includes(x.mod)) {
				let votes = (vote) ? 1 : 0,
					antivotes = (vote) ? 0 : 1;
				// Register player and add vote
				data.db.soldiers.once("value", function(snap) {
					dio.del(data.messageID, data);

					let newplayer = data.db.soldiers.child(lucky), // Sets a reference
						l = snap.val()[lucky], // Current votee's data
						voter = {
							mod: fromID,
							ticket: vote // true/false depending on key/nokey
						};

					// If the user doesn't exist - Theoretically shouldn't happen?
					if (!l) {
						memsnap[lucky].username = help.getNickFromId(fromID, data.bot);
						newplayer.set( memsnap[lucky] ); // Sets data for user
					}

					logger.log("Pushing vote to user", "Info");
					newplayer.child("vote").push(voter, (err) => { // Update with new vote
						if (err) {
							logger.log("Couldn't push vote to user. Oops.", "Error");
							return false;
						} else {
							if (!l) return false; // Abort if user was just added
							// Otherwise, get the total votes
							for ( let modvote in l.vote ) {
								if (l.vote[modvote].ticket) {
									votes++;
								} else { antivotes++; }
							}

							if (vote) {
								logger.log(`${from} voted for ${memsnap[lucky].username}!`, "Info");
								dio.say(`:key: ${from} has voted to key <@${lucky}>. \n User has **${votes-antivotes}** :tickets:`, data, x.history);
							} else {
								logger.log(`${from} voted against <@${lucky}>!`, "Info");
								dio.say(`:no_entry_sign: ${from} has voted against keying ${memsnap[lucky].username}. \n User has **${votes-antivotes}** :tickets:`, data, x.history);
							}
						}
					});
				}, function(err) {
					logger.log(err, "Error");
				});

				// Mod finished voting, we're done here
				return false;
			}

			// Developer gives a key immediately (unless its an antivote, then cancel?).
			if (!vote) return false;

			for (let code in kk) {
				// Removes key from Firebase
				keys.child(code).set({});
				giveKey(data,lucky,kk[code].key);

				switch(fromID) {
				case x.schatz:
					dio.say(`${x.emojis.schatz} gave <@${lucky}> a key.`, data, x.history);
					break;
				case x.dex:
					dio.say(`${x.emojis.dexter} gave <@${lucky}> a key.`, data, x.history);
					break;
				case x.nguyen:
					dio.say(`${x.emojis.nguyen} gave <@${lucky}> a key.`, data, x.history);
					break;
				case x.stealth:
					dio.say(`${x.emojis.masta} gave <@${lucky}> a key.`, data, x.history);
					break;
				}

				logger.log(`${from} has given a key.`, logger.MESSAGE_TYPE.OK);
			}
		}, function(err) {
			logger.log(err, logger.MESSAGE_TYPE.Error);
		});
	};

	cmdNoKey = new command("key", ["!nokey","!taketicket"], "Blocks a vote to key a member", function(data) {
		voteKey(data, false);
	});

	// cmdNoKey.permissions = [x.admin, x.mod];

	cmdKey = new command("key", ["!key","!giveticket"], "Adds a vote to key a member by mods, instakey by dev", function(data) {
		voteKey(data, true);
	});

	// cmdKey.permissions = [x.admin, x.mod];

	cmdWave = new command("key", "!wave", "Checks the current wave of lotto entrees", function(data) {
		data.db.soldiers.orderByChild("vote").startAt(1).once("value", function(snap) {
			dio.del(data.messageID, data);

			let ulist = snap.val(),
				ulistTxt = "";

			// Loop through users with votes
			for (let u in ulist) {
				let count = 0;
				// Tally votes
				for (let v in ulist[u].vote) {
					if (ulist[u].vote[v].ticket) { count += 1; } else { count -= 1; }
				}
				logger.log(`${count} | ${ulist[u].id}`, "Info");
				ulistTxt += `${(ulist[u].verified) ? "✓" : "-"} ${count} 🎫 | ${help.getNickFromId(ulist[u].id, data.bot)} \n`;
			}

			if (ulistTxt.length < 3) ulistTxt = "There are no entrees."; // Space if empty
			dio.say(`**:slot_machine: Current Lotto Entrees:** \`\`\`${ulistTxt}\`\`\``, data, x.history);
		});
	});

	cmdWave.permissions = [x.admin, x.mod];
}

module.exports.commands = [cmdKey, cmdNoKey, cmdWave];
