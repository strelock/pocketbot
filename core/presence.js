/* ----------------------------------------
	This file controls all the events fired
	off by presence changes from users, i.e.
	status, "Now playing...", and streams
 ---------------------------------------- */
var exports = module.exports = {};

let logger  = require("./logger"),
	dio		= require("./dio"),
	x 		= require("./vars"),
	req 	= require("request-promise-native"),
	TOKEN 	= require("../core/tokens"),
	helper	= require("./helpers");

// Timers
let sTimer = 0,
	dTimer = 0,
	fTimer = 0,
	mTimer = 0;

exports.onChange = async function(status, userdata=null) {
	let from = status.user,
		fromID = status.userID,
		game = status.game,
		timer = new Date().getTime();

	// Someone goes offline
	if (status.state === "offline") {
		// Check if they are on ready list
		let offliner = status.bot.servers[x.chan].members[fromID];

		if (offliner) { // I don't why this returns undefined sometimes?
			if (offliner.hasOwnProperty("roles") && offliner.roles.includes(x.lfg)) {
				status.bot.removeFromRole({
					serverID: x.chan,
					userID: fromID,
					roleID: x.lfg
				}, function(err, resp) {
					if (err) {
						logger.log(resp, logger.MESSAGE_TYPE.Error);
						return false;
					}

					dio.say(`Removing ${from} from ready list due to disconnect.`, status, x.chan);
				});
			}

			// D/c dev Timers
			switch (fromID) {
			case x.schatz:
				sTimer = new Date().getTime();
				break;
			case x.dex:
				fTimer = new Date().getTime();
				break;
			case x.adam:
				dTimer = new Date().getTime();
				break;
			case x.stealth:
				mTimer = new Date().getTime();
				break;
			}

			// Update user data on Firebase - Line 52 in userdata.js
			if ( status.bot.servers[x.chan].members.hasOwnProperty(fromID) ) {
				userdata.setProp({
					user: status.bot.servers[x.chan].members[fromID],
					prop: { name: "state" }
				});
			}
		}
	}
	let mem = status.bot.servers[x.chan].members[fromID];
	let fromRoles = [];
	if(mem) {
		fromRoles = (status.bot.servers[x.chan].members[fromID].roles) || null;
	}

	// Someone comes online
	if (status.state === "online") {

		//Dev greetings(and PR greetins, in debug mode)
		if ( fromRoles.includes(x.admin) ) {
			logger.log("Dev came online...", "Info");

			userdata.getProp(fromID, "status").then( (oldState) => {
				if((oldState == "offline" || oldState == null) && oldState != status.state){
					//TODO: Since getNickFromId got changed to default to a user's account name, maybe the check is unnecessary
					let nickname = (helper.getNickFromId(fromID, status.bot) ? helper.getNickFromId(fromID, status.bot) : from);
					let greets = [
						`Greetings Master ${nickname}`,
						`o/ ${nickname}`,
						`May your devness shine light upon us all, ${nickname}`,
						`One ${nickname} a day makes bugs go away. Welcome back!`,
						`It's Butters, not Butter, ${nickname}!`,
						`Sup ${nickname}`,
						`${nickname} has entered the building.`,
						`${nickname} has joined the fight!`
					];

					if (fromID === x.schatz) {
						greets.push(
							`How's the fam, Master Schatz? ${x.emojis.schatz}`,
							`Ah! welcome back, Master Schatz! ${x.emojis.schatz}`,
							`Good of you to join us, Master Schatz. ${x.emojis.schatz}`,
							`${x.emojis.schatz} Master Schatz, you've returned.`
						);

						if ( (timer - sTimer) < 1800000) return false;
					} else if (fromID === x.adam) {
						greets.push(
							`The pixel artist has returned. Welcome back Master Adam ${x.emojis.adam}.`,
							`Good of you to join us, Master Adam ${x.emojis.adam}.`,
							`${x.emojis.adam} Master DeGrandis, you've returned.`
						);

						if ( (timer - dTimer) < 1800000) return false;
					} else if (fromID === x.dex) {
						greets.push(
							`${x.emojis.dexter} Greetings Master Dexter, or should I say \`nasty girl\`? :speak_no_evil: `,
							`${x.emojis.dexter} Master Dexter, welcome back to your laboratory!`,
							`I see your afro remains dynamic, Master Dexter. ${x.emojis.dexter}`,
							`Back again I see, Master Dexter. ${x.emojis.dexter}`
						);

						if ( (timer - fTimer) < 1800000) return false;
					} else if (fromID === x.stealth) {
						greets.push(
							`Ah! welcome back, Webmaster Stealth! ${x.emojis.masta}`,
							`Good of you to join us, Master Masta. ${x.emojis.masta}`,
							`Greetings, Mastastealth! ${x.emojis.masta}`,
							`Oh look, a real dev! Heya ${nickname}`
						);

						if ( (timer - mTimer) < 1800000) return false;
					}

					let n = Math.floor( Math.random()*greets.length );
					dio.say(greets[n], status, x.chan);

					userdata.setProp({
						user: status.bot.servers[x.chan].members[fromID],
						prop: { name: "state" }
					});
				}
			}).catch( (err) => {
				logger.log(err, "Error");
			});
		} else {
			userdata.setProp({
				user: status.bot.servers[x.chan].members[fromID],
				prop: { name: "state" }
			});
		}
	}

	// Someone is playing/streaming (?) the game
	if (game) {
		let gameName = game.name.toLowerCase(),
			streamer = ( game.hasOwnProperty("url") ) ?  game.url.substr(game.url.lastIndexOf("/") + 1) : null,
			stream = "";

		// Let's get some stream info, if it exists
		if (streamer) await req({
			url: "https://api.twitch.tv/kraken/streams/"+streamer,
			headers: { "Client-ID": TOKEN.TWITCHID }
		}).then( (body)=> {
			streamer = JSON.parse(body).stream;
		}).catch( (err) => {
			logger.log(err, "Error");
		});

		// Check for game name
		if ( (gameName.includes("tooth") && gameName.includes("tail")) || (stream.includes("tooth") && stream.includes("tail")) ) {
			// Add to LFG
			status.bot.addToRole({
				serverID: x.chan,
				userID: fromID,
				roleID: x.lfg
			}, (err, resp) => {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					return false;
				}
				// And if the user is not a Recruit OR Veteran
				if (!fromRoles.includes(x.noob) && !fromRoles.includes(x.member)) {
					// Make em recruit
					status.bot.addToRole({
						serverID: x.chan,
						userID: fromID,
						roleID: x.noob
					});
				}
			});
		} else {
			// If he's not playing/streaming it, and has LFG, remove
			if (fromRoles.includes(x.lfg)) {
				status.bot.removeFromRole({
					serverID: x.chan,
					userID: fromID,
					roleID: x.lfg
				});
			}
		}
	} else {
		// Or if he stopped playing/streaming, remove LFG
		if (fromRoles.includes(x.lfg)) {
			status.bot.removeFromRole({
				serverID: x.chan,
				userID: fromID,
				roleID: x.lfg
			});
		}
	}

	// Log all changes in debug mode
	if ( helper.isDebug() ) logger.log(`User '${from}' is now '${status.state}'`);
};
